package pl.sda.hibernate.starter;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import pl.sda.hibernate.starter.entities.CourseEntity;
import pl.sda.hibernate.starter.entities.StudentEntity;

public class HibernateMetadataSources {
    public static void main(String[] args) {
        ServiceRegistry serviceRegistry =
                new StandardServiceRegistryBuilder().configure().build();

        SessionFactory sessionFactory = new MetadataSources(serviceRegistry)
                .addAnnotatedClass(StudentEntity.class)
                .addAnnotatedClass(CourseEntity.class)
                .buildMetadata()
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        StudentEntity student =
                new StudentEntity("Jacek", 10, "Opis musi byc", "1_2_3");

        session.save(student);



        session.close();

    }
}
