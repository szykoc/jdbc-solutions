package pl.sda.jdbc.starter.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.jdbc.starter.ConnectionFactory;
import pl.sda.jdbc.starter.dto.CoursesDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CoursesDAO {

    private static Logger LOG = LoggerFactory.getLogger(CoursesDAO.class);
    private ConnectionFactory connectionFactory;

    public void deleteByCourseId(int id) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        try (Connection connection = connectionFactory.getConnection()) {

            LOG.info("Delete course by id: {}", id);

            String sql = "DELETE FROM courses WHERE id=?";

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            statement.executeUpdate();
            statement.close();

        } catch (SQLException ex) {

        }
    }

    public List<CoursesDTO> printAllCourses() {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        try (Connection connection = connectionFactory.getConnection()) {
            LOG.info("Printing all courses method");

            String sql = "SELECT * FROM courses";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();

            List<CoursesDTO> allCourses = new ArrayList<>();
            while (resultSet.next()){
                CoursesDTO coursesDTO = new CoursesDTO();
                coursesDTO.setId(resultSet.getInt("id"));
                coursesDTO.setName(resultSet.getString("name"));
                coursesDTO.setPlace(resultSet.getString("place"));
                coursesDTO.setStartDate(LocalDate.parse(resultSet.getString("start_date")));
                coursesDTO.setEndDate(LocalDate.parse(resultSet.getString("end_date")));
                allCourses.add(coursesDTO);
            }
            return allCourses;
        } catch (SQLException ex){
            LOG.error("Database error");
        }
        return null;
    }
}
