package pl.sda.jdbc.starter.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.jdbc.starter.ConnectionFactory;
import pl.sda.jdbc.starter.dto.StudentDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentsDAO {

    private static Logger LOG = LoggerFactory.getLogger(StudentsDAO.class);
    private ConnectionFactory connectionFactory;

    public void addStudent(){

    }

    public void deleteStudent(){

    }

    public void updateStudent(int id, String description, String seat){
        ConnectionFactory connectionFactory = new ConnectionFactory();
        try(Connection connection = connectionFactory.getConnection()){

            LOG.info("Updating student by id: {}", id);

            String sql = "UPDATE sda_courses.students SET description=?, seat=? WHERE id=?";

            PreparedStatement statement =  connection.prepareStatement(sql);
            statement.setString(1, description);
            statement.setString(2, seat);
            statement.setInt(3, id);

            statement.executeUpdate();
            statement.close();
        } catch (SQLException ex){

        }
    }

    public List<StudentDTO> printAll() {
        ConnectionFactory factory = new ConnectionFactory();
        try(Connection connection = factory.getConnection()){
            Statement statement = connection.createStatement();

            String sql = "SELECT s.id, s.name, s.description, s.seat, c.name AS course_name" +
                    " FROM students AS s" +
                    " LEFT JOIN courses AS c ON s.course_id = c.id";


            ResultSet dataFromDatabase = statement.executeQuery(sql);

            List<StudentDTO> students = new ArrayList<>();

            while (dataFromDatabase.next()) {
                int id = dataFromDatabase.getInt("id");
                String name = dataFromDatabase.getString("name");
                String description = dataFromDatabase.getString("description");
                String seat = dataFromDatabase.getString("seat");
                String courseName = dataFromDatabase.getString("course_name");
                courseName = (courseName == null) ? "brak kursu" : courseName;

                StudentDTO student = new StudentDTO();
                student.setId(id);
                student.setName(name);
                student.setDescription(description);
                student.setSeat(seat);
                students.add(student);

                LOG.info("Printing all Students: ");
                LOG.info("{} - ,  - {},  - {},  - {} , - {}", id, name, description, seat, courseName);
            }

            return students;

        } catch (SQLException ex){

        }
        return null;
    }
}
