package pl.sda.jdbc.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.jdbc.starter.dao.CoursesDAO;
import pl.sda.jdbc.starter.dao.StudentsDAO;
import pl.sda.jdbc.starter.dto.CoursesDTO;
import pl.sda.jdbc.starter.dto.StudentDTO;

import java.sql.*;
import java.util.List;

public class CoursesManager {

    private static Logger LOG = LoggerFactory.getLogger(CoursesManager.class);
    private ConnectionFactory connectionFactory;
    private StudentsDAO studentsDAO;
    private CoursesDAO coursesDAO;


    public void createCoursesTable() {
        LOG.info("createCoursesTable method init");
        ConnectionFactory factory = new ConnectionFactory();
        try(Connection connection = factory.getConnection()){
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.courses (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  place VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  start_date DATE NOT NULL,\n" +
                    "  end_date DATE NOT NULL,\n" +
                    "PRIMARY KEY (id))\n" );

            LOG.info("Table: sda_courses.courses created!");

        } catch (SQLException e){

        }

    }

    public void createStudentsTable() {
        LOG.info("createStudentsTable method init");
        ConnectionFactory factory = new ConnectionFactory();
        try(Connection connection = factory.getConnection()){
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.students (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  description VARCHAR(200) DEFAULT NULL,\n" +
                    "  seat VARCHAR(10) DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n" +
                    " ENGINE = InnoDB;");

            LOG.info("Table: sda_courses.students created!");


        } catch (SQLException e){

        }
    }

    public void createAttendanceListTable() {
        LOG.info("createAttendanceListTable method init");
        ConnectionFactory factory = new ConnectionFactory();
        try(Connection connection = factory.getConnection()){
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.attendance_list(\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  student_id INT DEFAULT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  date DATETIME DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (student_id) REFERENCES sda_courses.students(id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n" +
                    "ENGINE = InnoDB;");

            LOG.info("Table: sda_courses.attendance_list created!");

        } catch (SQLException e){

        }
    }

    public void dropAllTables() {
        LOG.info("dropAllTables method init");
        ConnectionFactory factory = new ConnectionFactory();
        try(Connection connection = factory.getConnection()){
            Statement statement = connection.createStatement();
            statement.executeUpdate("DROP TABLE sda_courses.attendance_list");
            statement.executeUpdate("DROP TABLE sda_courses.students");
            statement.executeUpdate("DROP TABLE sda_courses.courses");
            LOG.info("All Tables dropped");
        }catch (SQLException e){

        }

    }

    public void printAllStudents(){
        studentsDAO = new StudentsDAO();
        List<StudentDTO> studentDTOS = studentsDAO.printAll();
        LOG.info(studentDTOS.toString());
    }

    public void updateStudentById(int id, String description, String seat){
        studentsDAO = new StudentsDAO();
        studentsDAO.updateStudent(id, description, seat);
    }

    public void deleteCourseById(int id){
        coursesDAO = new CoursesDAO();
        coursesDAO.deleteByCourseId(id);
    }

    public void printAllCourses(){
        coursesDAO = new CoursesDAO();
        List<CoursesDTO> allCourses = coursesDAO.printAllCourses();
    }

    public static void main(String[] args) {
        CoursesManager coursesManager = new CoursesManager();
        //coursesManager.dropAllTables();
        //coursesManager.createCoursesTable();
        //coursesManager.createStudentsTable();
        //coursesManager.createAttendanceListTable();
        coursesManager.printAllStudents();

        //coursesManager.updateStudentById(1, "opis", "3_2_1");

        //coursesManager.printAllStudents();
    }
}
