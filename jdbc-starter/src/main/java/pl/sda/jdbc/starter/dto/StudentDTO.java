package pl.sda.jdbc.starter.dto;

public class StudentDTO {
    private int id;
    private int courseId;
    private String name;
    private String description;
    private String seat;

    public int getId() {
        return id;
    }

    public StudentDTO setId(int id) {
        this.id = id;
        return this;
    }

    public int getCourseId() {
        return courseId;
    }

    public StudentDTO setCourseId(int courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getName() {
        return name;
    }

    public StudentDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public StudentDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getSeat() {
        return seat;
    }

    public StudentDTO setSeat(String seat) {
        this.seat = seat;
        return this;
    }

    @Override
    public String toString() {
        return "StudentDTO{" +
                "id=" + id +
                ", courseId=" + courseId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", seat='" + seat + '\'' +
                '}';
    }
}
