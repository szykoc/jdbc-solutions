package pl.sda.jdbc.starter.dto;

import java.time.LocalDate;

public class CoursesDTO {
    private int id;
    private String name;
    private String place;
    private LocalDate startDate;
    private LocalDate endDate;

    public int getId() {
        return id;
    }

    public CoursesDTO setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CoursesDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getPlace() {
        return place;
    }

    public CoursesDTO setPlace(String place) {
        this.place = place;
        return this;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public CoursesDTO setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public CoursesDTO setEndDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    @Override
    public String toString() {
        return "CoursesDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", place='" + place + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
